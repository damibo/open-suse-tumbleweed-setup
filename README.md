
# OpenSUSE Tumbleweed KDE setup

(c) Niki Kovacs 2022 

This repository provides:

* `setup.sh`: a post-installation setup script for OpenSUSE Tumbleweed KDE. 

* `upgrade.sh`: a script to upgrade OpenSUSE Leap (15.2, 15.3 or 15.4) to
  Tumbleweed.

## The perfect Linux desktop in a nutshell

Perform the following steps.

  1. Install OpenSUSE Tumbleweed KDE.

  2. Open a terminal (Konsole) as `root` (`su -`).

  3. Install Git: `zypper install --no-recommends git`

  4. Grab the script: `git clone https://gitlab.com/kikinovak/opensuse`

  5. Change into the new directory: `cd opensuse`

  6. Run the script: `./setup.sh --setup`

  7. Grab a cup of coffee while the script does all the work.

  8. Log out and log back in.


## Upgrade your existing OpenSUSE Leap installation

If you're running OpenSUSE Leap, use the script to upgrade to OpenSUSE
Tumbleweed.

  1. Boot into multi-user mode: `systemctl set-default multi-user.target && reboot`

  2. Install Git: `zypper install --no-recommends git`

  3. Grab the script: `git clone https://gitlab.com/kikinovak/opensuse`

  5. Change into the new directory: `cd opensuse`

  6. Run the script: `./upgrade.sh`

  7. Grab a cup of coffee while the script does all the work.

  8. Reboot and test your graphical environment: `systemctl isolate graphical.target`

  9. Boot into graphical mode: `systemctl set-default graphical.target`

> Currently the script manages OpenSUSE Leap 15.2, 15.3 and 15.4.

During the upgrade operation, you can view all the gory details in a second
terminal:

```
$ tail -f /var/log/upgrade.log
```


## Customizing a Linux desktop

Turning a vanilla Linux installation into a full-blown desktop with bells and
whistles always boils down to a series of more or less time-consuming
operations:

  * Customize the Bash shell: prompt, aliases, etc.

  * Customize the Vim editor.

  * Setup official and third-party repositories.

  * Remove some unneeded applications.

  * Install all missing applications.

  * Enhance multimedia capabilities with various codecs and plugins.

  * Install Microsoft and Eurostile fonts for better interoperability.

  * Edit application menu entries.

  * Configure the KDE desktop for better usability.

The `setup.sh` script performs all of these operations and some more.

Configure Bash, Vim and Xterm:

```
# ./setup.sh --shell
```

Setup official and third-party repositories:

```
# ./setup.sh --repos
```

Update system with enhanced packages and clean menus:

```
# ./setup.sh --fresh
```

Remove unneeded applications:

```
# ./setup.sh --strip
```

Install extra tools and applications:

```
# ./setup.sh --extra
```

Install Microsoft and Eurostile fonts:

```
# ./setup.sh --fonts
```

Configure custom menu entries:

```
# ./setup.sh --menus
```

Install custom KDE profile:

```
# ./setup.sh --kderc
```

Apply custom KDE profile for existing users:

```
# ./setup.sh --users
```

Perform all of the above in one go:

```
# ./setup.sh --setup
```

Install DVD/RW-related applications:

```
# ./setup.sh --dvdrw
```

Display an overview of all available options:

```
# ./setup.sh --help
```

If you want to know what exactly goes on under the hood, open a second terminal
and view the logs:

```
$ tail -f /var/log/setup.log
```

---

<p align="center"><small><em>Developed and written by <a
href="https://tinyurl.com/2p8tk495">Nicolas Kovacs</a>.<br /> Click on the cup
to buy me a coffee.</em></small></p>
<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

