#!/bin/bash
#
# upgrade.sh
#
# (c) Niki Kovacs 2022 <info@microlinux.fr>
#
# Upgrade OpenSUSE Leap (15.2, 15.3 or 15.4) to Tumbleweed

CWD=$(pwd)

TARGET="$(systemctl list-units --type target | \
  egrep 'emergency|rescue|graphical|multi-user' | \
	head -1 | xargs | awk -F. '{print $1}')"

PARAMS="--download in-advance \
	--auto-agree-with-licenses \
	--allow-vendor-change \
	--replacefiles \
	--recommends \
	--force-resolution \
	--no-confirm"

LOG="/var/log/$(basename "${0}" .sh).log"
echo > ${LOG}

LANG=en_US.UTF-8

SLEEP=1

if [[ "${UID}" -ne 0 ]]
then

  echo
  echo "  Please run with sudo or as root." >&2
  echo
  sleep ${SLEEP}
  exit 1

fi

source /etc/os-release

if [ "${?}" -ne 0 ]
then

  echo 
  echo "  Unsupported operating system." >&2
  echo
  sleep ${SLEEP}
  exit 1

elif [ "${NAME}" != "openSUSE Leap" ]
then
  
  echo
  echo "  Please run this script on OpenSUSE Leap only." >&2
  echo 
  sleep ${SLEEP}
  exit 1

fi

case "${VERSION}" in
  "42.1"|"42.2"|"42.3"|"15.0"|"15.1")
    echo
    echo "  OpenSUSE Leap ${VERSION} is not supported."
    echo
    exit 1
  ;;
  "15.2"|"15.3"|"15.4")
    echo
    echo "  ##############################################"
    echo "  # Upgrade: OpenSUSE Leap ${VERSION} --> Tumbleweed #"
    echo "  ##############################################"
    echo
    echo "  Logs are written to ${LOG}."
    echo
    sleep ${SLEEP}
  ;;
  *)
    echo
    echo "  OpenSUSE Leap ${VERSION} is not supported." >&2
    echo
    exit 1
  ;;
esac

case "${TARGET}" in
  "graphical"|"rescue"|"emergency")
    echo "  Current boot target: ${TARGET}.target."
    echo "  Please switch to multi-user.target."
    echo
    exit 1
  ;;
  "multi-user")
    echo "  Current boot target: ${TARGET}.target."
    echo
  ;;
  *)
    echo "  Unknown boot target."
    echo
    exit 1
  ;;
esac

# Package repositories
REPOS="oss non-oss updates"

echo "  === Package repository configuration ==="
echo
sleep ${SLEEP}

echo "  Removing existing repositories."
rm -f /etc/zypp/repos.d/*.repo*
sleep ${SLEEP}

for REPO in ${REPOS}
do
  echo "  Configuring repository: ${REPO}"
  cp -v ${CWD}/repos.d/${REPO}.repo /etc/zypp/repos.d/ >> ${LOG} 2>&1
  sleep ${SLEEP}
done

echo "  Refreshing repository information."
sleep ${SLEEP}
echo "  This might take a moment..."
zypper --gpg-auto-import-keys refresh >> ${LOG} 2>&1

echo "  Upgrading package management tools."
sleep ${SLEEP}
zypper patch ${PARAMS} --updatestack-only >> ${LOG} 2>&1

echo "  Upgrading system."
sleep ${SLEEP}
echo "  Go grab some coffee..."
zypper dist-upgrade ${PARAMS} >> ${LOG} 2>&1

echo "  Checking for orphaned packages."
ORPHANS=$(zypper packages --orphaned | grep @System | cut -d '|' -f 3)
if [ ! -z "${ORPHANS}" ]
then
  echo "  Removing orphaned packages."
  sleep ${SLEEP}
  echo "  Almost there..."
  zypper remove -y ${ORPHANS} >> ${LOG} 2>&1 
else
  echo "  No orphaned packages found."
fi

echo "  System upgrade complete."
sleep ${SLEEP}
echo "  Please reboot."

echo

exit 0
